# Design Docs

## Templates

* Shopping Cart
* Shipping: Guest
* Shipping: User
* Billing: Guest
* Billing: User
* Review & Place Order
* Order Confirmation
* Product Listing showing Modal: Add to Cart & Mini-Cart Dropdown
* Policy Link Modal


## User interactions that don't require page reload, but do need to make server-side AJAX calls

### Shipping and Billing

#### City and State lookup 

Upon 5 numeric digits input into zip code field, send GET request to server passing zip code value, show loading graphic and message

* On success: populate city and state fields with returned values
* On zero results or error: hide loading graphic and message

### Shipping and Review/Place Order

#### Change shipping method

* update order subtotal and total
* if next business day chosen, show appropriate message about 1pm order deadline

_ _ _

## Server-side API services needed
### City and State Lookup (type: GET)

#### Request params

* zip_code

#### Success: Response

* city
* state

[JSON response](https://gist.github.com/jferrel7/f62834dca6d0eac58176#file-city-and-state-lookup)











