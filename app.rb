require 'sinatra'

get '/' do
  @filename = params[:filename] || '01-cart'
  @height = params['h'] || 1340
  erb :design, :layout => !request.xhr?
end

get '/:filename' do
  @filename = params[:filename]
  @height = params['h']
  @overlay = params['o']
  erb :design, :layout => !request.xhr?
end